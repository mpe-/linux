#!/bin/bash

echo "## ARCH          = $ARCH"
echo "## CROSS_COMPILE = $CROSS_COMPILE"
echo "## KBUILD_OUTPUT = $KBUILD_OUTPUT"

export DEFCONFIG=${DEFCONFIG:-ppc64le_defconfig}
echo "## DEFCONFIG     = $DEFCONFIG"

JFACTOR=${JFACTOR:-$(nproc)}
echo "## JFACTOR       = $JFACTOR"

rc=0

if [[ ${BUILD_KERNEL} != 0 ]]; then
    set -x
    make -s $DEFCONFIG
    make -s -j ${JFACTOR:-$(nproc)} ${MAKE_TARGET}
    rc=$?
    set +x

    echo "## Kernel build completed rc = $rc"
fi

if [[ $rc -eq 0 && ${BUILD_SELFTESTS} == 1 ]]; then
    set -x
    make -s -j ${JFACTOR:-$(nproc)} -C tools/testing/selftests TARGETS=powerpc
    rc=$?
    set +x
    echo "## Selftest build completed rc = $rc"
fi

if [[ $rc -ne 0 ]]; then
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "!! Error build failed rc $rc"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
else
    echo "## Build completed OK"
fi

exit $rc
